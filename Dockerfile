FROM python:3.8
WORKDIR /home/project

COPY requirements.txt ./
RUN apt update && pip3 install -r ./requirements.txt

COPY ./ ./
EXPOSE 5000

ENV FLASK_APP=app.py
ENTRYPOINT flask run -h 0.0.0.0